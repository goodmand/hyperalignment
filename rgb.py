import matplotlib.pyplot as plt
import numpy as np
import random
from scipy.spatial import procrustes
import imageio.v3 as image
import hypertools as hyp
import time

def toGray(img):
    the_image = [ [0]*len(img[0]) for i in range(len(img))]
    for i in range(len(img[0])):
        for j in range(len(img)):
            the_image[i][j] = img[i][j][0] * 0.2989 + img[i][j][1] * 0.5870 + img[i][j][2] * 0.1140
    return the_image

init_img = image.imread('im1.jpg')
init_img2 = image.imread('im2.jpg')

# R = np.reshape(init_img[:,:,2], (len(init_img[0]), len(init_img)))
R = init_img[:,:,0]
R2 = init_img2[:,:,0]

G = init_img[:,:,1]
G2 = init_img2[:,:,1]

B = init_img[:,:,2]
B2 = init_img2[:,:,2]

print("1")
start = time.time()
res = hyp.align([R, R2])
res2 = hyp.align([G, G2])
res3 = hyp.align([B, B2])
print(time.time() - start)
print("2")

final_image = np.zeros(init_img.shape)
final_image[:,:,0] = res[0]
final_image[:,:,1] = res2[0]
final_image[:,:,2] = res3[0]

final_image2 = np.zeros(init_img.shape)
final_image2[:,:,0] = res[1]
final_image2[:,:,1] = res2[1]
final_image2[:,:,2] = res3[1]


f, axarr = plt.subplots(1,4)

axarr[0].imshow(init_img)
axarr[0].title.set_text("image 1")

axarr[1].imshow(init_img2)
axarr[1].title.set_text("image 2")

axarr[2].imshow((final_image.astype(int)))
axarr[2].title.set_text("aligned 1")

axarr[3].imshow((final_image2.astype(int)), cmap='Reds', vmin=0, vmax=255)
axarr[3].title.set_text("aligned 2")

# hyp.plot([np.array(init_img[0]), np.array(init_img2)], legend=["image 1", "image 2"], title="before alignment")
# hyp.plot([res[0], res[1]], legend=["aligned image 1", "aligned image 2"], title="after alignment")    

plt.show()