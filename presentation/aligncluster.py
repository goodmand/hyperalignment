from configparser import Interpolation
import hypertools as hyp
import numpy as np
import matplotlib.pyplot as plt
import os
import imageio.v3 as image

# Remove folders & length divisible by three
def format_list(arr):
    for i in range(len(arr)-1):
        if '.' not in arr[i]:
            arr.pop(i)
    for i in range(len(arr) % 3):
        arr.pop()

# Return batch of three images
def tri_image_read(names, start):
    list = ["samplename.png"]*3
    for i in range(3):
        list[i] = image.imread("juicer/" + str(start + i) + ".png")
    return list

def toGray(img):
    the_image = [ [0]*len(img[0]) for i in range(len(img))]
    for i in range(len(img[0])):
        for j in range(len(img)):
            the_image[i][j] = img[i][j][0] * 0.2989 + img[i][j][1] * 0.5870 + img[i][j][2] * 0.1140
    return the_image

def tri_align_images(images, num_plot):
    im1 = np.array(toGray(images[0]))
    im2 = np.array(toGray(images[1]))
    im3 = np.array(toGray(images[2]))

    hyp.plot([im1, im2, im3], legend=["Sensor 1", "Sensor 2", "Sensor 3"], title="before alignment", save_path=("plots/b" + str(i) + ".png"))    
    res = hyp.align([im1, im2, im3])
    hyp.plot([res[2]], legend=["aligned result"], title="after alignment", save_path=("plots/a" + str(i) + ".png"))
    return res 

fs = os.listdir("juicer/")
print(fs)
format_list(fs)
files = sorted(fs, key=lambda x: int(os.path.splitext(x)[0]))

fig = plt.figure()
fig.patch.set_facecolor('#136170')
nums = [1, 6, 13, 14, 22]
for i in range(int(len(files)/3)):
# for i in nums:

    init_images = [np.zeros((200,200)), np.zeros((200,200)), np.zeros((200,200))]
    init_images = tri_image_read(files, (3*i) + 1)

    for k in range(3):
        ax = fig.add_subplot(231 + k)
        ax.imshow(init_images[k])
        ax.title.set_text("image " + str(k + 1))
        ax.axis('off')

    res = tri_align_images(init_images, i)

    res = hyp.align([np.array(toGray(init_images[0])), np.array(toGray(init_images[1])), np.array(toGray(init_images[2]))])
    ax1 = fig.add_subplot(235)
    ax1.imshow(res[2].astype(int), cmap='gray', vmin=0, vmax=255)
    ax1.title.set_text("aligned image")
    ax1.axis('off')

    plt.savefig("new" + str(i+1) + ".png")
    plt.clf()