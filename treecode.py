import matplotlib.pyplot as plt
import numpy as np
import random
from scipy.spatial import procrustes
import imageio.v3 as image
import hypertools as hyp

def toGray(img):
    the_image = [ [0]*len(img[0]) for i in range(len(img))]
    for i in range(len(img[0])):
        for j in range(len(img)):
            the_image[i][j] = img[i][j][0] * 0.2989 + img[i][j][1] * 0.5870 + img[i][j][2] * 0.1140
    return the_image


img = image.imread('wintree1.jpg')
img2 = image.imread('wintree2.jpg')
img3 = image.imread('wintree3.jpg')
img4 = image.imread('wintree4.jpg')

gray_img = toGray(img)
gray_img2 = toGray(img2)
gray_img3 = toGray(img3)
gray_img4 = toGray(img4)

res = hyp.align([np.array(gray_img), np.array(gray_img2), np.array(gray_img3), np.array(gray_img4)])
f, axarr = plt.subplots(1,8)

axarr[0].imshow(gray_img, cmap='gray', vmin=0, vmax=255)
axarr[0].title.set_text("image 1")

axarr[1].imshow(gray_img2, cmap='gray', vmin=0, vmax=255)
axarr[1].title.set_text("image 2")

axarr[2].imshow(gray_img3, cmap='gray', vmin=0, vmax=255)
axarr[2].title.set_text("image 3")

axarr[3].imshow(gray_img4, cmap='gray', vmin=0, vmax=255)
axarr[3].title.set_text("image 3")

axarr[4].imshow((res[0]), cmap='gray', vmin=0, vmax=255)
axarr[4].title.set_text("aligned 1")

axarr[5].imshow((res[1]), cmap='gray', vmin=0, vmax=255)
axarr[5].title.set_text("aligned 2")

axarr[6].imshow((res[2]), cmap='gray', vmin=0, vmax=255)
axarr[6].title.set_text("aligned 3")

axarr[7].imshow((res[3]), cmap='gray', vmin=0, vmax=255)
axarr[7].title.set_text("aligned 3")

hyp.plot([np.array(gray_img), np.array(gray_img2), np.array(gray_img3), np.array(gray_img4)], legend=["image 1", "image 2", "image 3", "image 4"], title="before alignment")
hyp.plot([res[0], res[1], res[2], res[3]], legend=["aligned image 1", "aligned image 2", "aligned image 3", "aligned image 4"], title="after alignment")    

plt.show()