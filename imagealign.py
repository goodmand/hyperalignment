import hypertools as hyp
from scipy import misc
import numpy as np
import imageio
import time

img1 = imageio.v3.imread("bird.jpg")
img2 = imageio.v3.imread("bird.jpg")



h = len(img1)
w = len(img1[0])

reshape_1 = np.reshape(img1, (w*h, 3))
reshape_2 = np.reshape(img2, (w*h, 3))
np.savetxt("logs/source1.csv", reshape_1, delimiter=",")
data_to_align = [reshape_1, reshape_2]

start = time.time()    
aligned_data = hyp.align(data_to_align)
print("Time:", time.time() - start, "sec")
group1 , group2 = aligned_data
np.savetxt("logs/result1.csv", group1, delimiter=",")
new_img1 = np.reshape(group1, (h, w, 3))
new_img2 = np.reshape(group2, (h, w, 3))

np.savetxt("logs/result2.csv", new_img1[0].astype(np.uint8), delimiter=",")

imageio.v3.imwrite("aligned_image1.png", new_img1.astype(np.uint8))
imageio.v3.imwrite("aligned_image2.png", new_img2.astype(np.uint8))
