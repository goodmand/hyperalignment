import matplotlib.pyplot as plt
import numpy as np
import random
from scipy.spatial import procrustes
import imageio.v3 as image
import hypertools as hyp

def toGray(img):
    the_image = [ [0]*len(img[0]) for i in range(len(img))]
    for i in range(len(img[0])):
        for j in range(len(img)):
            the_image[i][j] = img[i][j][0] * 0.2989 + img[i][j][1] * 0.5870 + img[i][j][2] * 0.1140
    return the_image

im1 = image.imread('wintree1.jpg')
im2 = image.imread('wintree2.jpg')
im3 = image.imread('wintree3.jpg')
im4 = image.imread('wintree4.jpg')

gr1 = [np.array(toGray(im1)), np.array(toGray(im2))]
gr2 = [np.array(toGray(im3)), np.array(toGray(im4))]
data = np.array([gr1, gr2])

res = hyp.align(data)
f, axarr = plt.subplots(1,4)

a_img = np.reshape(res[0], (len(init_img[0]),len(init_img), 3))
a_img2 = np.reshape(res[1], (len(init_img[0]),len(init_img), 3))

axarr[0].imshow(init_img)
axarr[0].title.set_text("image 1")

axarr[1].imshow(init_img2)
axarr[1].title.set_text("image 2")

axarr[2].imshow(a_img.astype(int))
axarr[2].title.set_text("aligned 1")

axarr[3].imshow(a_img2.astype(int))
axarr[3].title.set_text("aligned 2")

hyp.plot([np.array(img), np.array(img2)], legend=["image 1", "image 2"], title="before alignment")
hyp.plot([res[0], res[1]], legend=["aligned image 1", "aligned image 2"], title="after alignment")    

plt.show()